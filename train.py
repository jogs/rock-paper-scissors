import os
import cv2
import numpy as np
import tensorflow as tf
import tensorflow.keras.utils as np_utils
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dropout, GlobalAveragePooling2D, Dense
# from keras.models import Sequential
from tensorflow.keras.applications.nasnet import NASNetMobile

IMG_SAVE_PATH = 'image_data'

CLASS_MAP = {
    "rock": 0,
    "paper": 1,
    "scissors": 2,
    "none": 3
}

NUM_CLASSES = len(CLASS_MAP)

# This is the input size which our model accepts.
image_size = 224


def mapper(val):
    return CLASS_MAP[val]


# def get_model():
#     model = Sequential([
#         SqueezeNet(input_shape=(227, 227, 3), include_top=False),
#         Dropout(0.5),
#         Convolution2D(NUM_CLASSES, (1, 1), padding='valid'),
#         Activation('relu'),
#         GlobalAveragePooling2D(),
#         Activation('softmax')
#     ])
#     return model

def get_model():
    # Loading pre-trained NASNETMobile Model without the head by doing include_top = False
    n_mobile = NASNetMobile( input_shape=(image_size, image_size, 3), include_top=False, weights='imagenet')

    # Freeze the whole model 
    n_mobile.trainable = False
        
    # Adding our own custom head
    # Start by taking the output feature maps from NASNETMobile
    x = n_mobile.output

    # Convert to a single-dimensional vector by Global Average Pooling. 
    # We could also use Flatten()(x) GAP is more effective reduces params and controls overfitting.
    x = GlobalAveragePooling2D()(x)

    # Adding a dense layer with 712 units
    x = Dense(712, activation='relu')(x) 

    # Dropout 40% of the activations, helps reduces overfitting
    x = Dropout(0.40)(x)

    # The fianl layer will contain 4 output units (no of units = no of classes) with softmax function.
    preds = Dense(4 ,activation='softmax')(x) 

    # Construct the full model
    model = tf.keras.Model(inputs=n_mobile.input, outputs=preds)

    # Check the number of layers in the final Model
    print ("Number of Layers in Model: {}".format(len(model.layers[:])))

    return model


# load images from the directory
dataset = []
for directory in os.listdir(IMG_SAVE_PATH):
    path = os.path.join(IMG_SAVE_PATH, directory)
    if not os.path.isdir(path):
        continue
    for item in os.listdir(path):
        # to make sure no hidden files get in our way
        if item.startswith("."):
            continue
        img = cv2.imread(os.path.join(path, item))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (image_size, image_size))
        dataset.append([img, directory])

'''
dataset = [
    [[...], 'rock'],
    [[...], 'paper'],
    ...
]
'''
data, labels = zip(*dataset)
labels = list(map(mapper, labels))


'''
labels: rock,paper,paper,scissors,rock...
one hot encoded: [1,0,0], [0,1,0], [0,1,0], [0,0,1], [1,0,0]...
'''

# one hot encode the labels
labels = np_utils.to_categorical(labels)

# define the model
model = get_model()
model.compile(
    optimizer=Adam(learning_rate=0.0003),
    loss='categorical_crossentropy',
    metrics=['accuracy']
)

# start training
model.fit(np.array(data), np.array(labels), epochs=15)

# save the model for later use
model.save("rock-paper-scissors-model.h5")
