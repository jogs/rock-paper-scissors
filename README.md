# rock-paper-scissors

https://youtu.be/0uSA3xyXlwM

An AI to play the Rock Paper Scissors game

## Requirements
- Python 3
- Keras
- Tensorflow 2
- OpenCV

## Set up instructions
1. Clone the repo.
```sh
$ git clone https://gitlab.com/jogs/rock-paper-scissors
$ cd rock-paper-scissors
```

+ _(OPTIONAL) install ubuntu dependencies_
```sh
$ make ubuntu-preinstall
```

2. Install the dependencies
```sh
$ make install
```

3. Gather Images for each gesture (rock, paper and scissors and None):
In this example, we gather 180 images for the "rock" gesture
```sh
$ make collect-rock 
```

4. Train the model
```sh
$ rm -rf image_data/test
$ make train
```

5. Test the model on some images
```sh
$ make collect-test
$ make test image-path=./image_data/test/1.jpg
```

6. Play the game with your computer!
```sh
$ make play
```
