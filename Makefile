ubuntu-preinstall:
	sudo apt update && \
	sudo apt install build-essential cmake git pkg-config libgtk-3-dev \
    libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
    libxvidcore-dev libx264-dev libjpeg-dev libpng-dev libtiff-dev \
    gfortran openexr libatlas-base-dev python3-dev python3-numpy \
    libtbb2 libtbb-dev libdc1394-22-dev python3 python3.pip \
		python3-opencv python3-testresources && \
		python -m pip install --upgrade setuptools

install:
	pip install -r requirements.txt

collect-rock:
	python3 gather_images.py rock 180

collect-scissors:
	python3 gather_images.py scissors 180

collect-paper:
	python3 gather_images.py paper 180

collect-none:
	python3 gather_images.py none 180

collect-test:
	python3 gather_images.py test 1

train:
	python3 train.py

test:
	python3 test.py $(image-path)

play:
	python3 play.py